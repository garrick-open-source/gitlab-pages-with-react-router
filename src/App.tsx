import { HashRouter, Switch, Route, Link } from "react-router-dom";
import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Gitlab Pages With React Router</p>
        <HashRouter basename="/">
          <Switch>
            <Route exact path="/">
              <Link to="/second-path"> Second Path</Link>
              <Link to="/third-path"> Third Path</Link>
              <p>I am Home Path</p>
            </Route>
            <Route path="/second-path">
              <Link to="/"> Home Path</Link>
              <Link to="/third-path"> Third Path</Link>
              <p>I am Second Path</p>
            </Route>
            <Route path="/third-path">
              <Link to="/"> Home Path</Link>
              <Link to="/third-path"> Third Path</Link>
              <p>I am Third Path</p>
            </Route>
          </Switch>
        </HashRouter>
      </header>
    </div>
  );
}

export default App;
